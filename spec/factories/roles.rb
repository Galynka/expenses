FactoryGirl.define do
  factory :role, class: 'Role' do
    name 'guest'

    trait :customer_role do
      name 'customer'
    end

    trait :employee_role do
      name 'employee'
    end
  end
end

