FactoryGirl.define do
  factory :user, class: 'User' do
    password '12345678'
    password_confirmation '12345678'
    sequence(:email) { |n| "person#{n}@example.com" }

    trait :customer do
      role { create :role, :customer_role }
    end

    trait :employee do
      role { create :role, :employee_role }
    end
  end
end

