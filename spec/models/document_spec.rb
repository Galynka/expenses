require 'rails_helper'

RSpec.describe Document, type: :model do
  it "is valid with date" do
    document = FactoryGirl.create :document, date: "25 November 2015"

    expect(document).to be_valid
  end

  it "is valid with users currency" do
    document = create :document
    user = create :user
    document.user = user
    currency = create :currency
    user.currencies.append currency
    document.currency = currency

    expect(document).to be_valid
  end

  it "is valid with blank currency" do
    document = create :document, currency: nil

    expect(document).to be_valid
  end

  it 'is invalid with nonexistent currency id' do
    document = build :document, user: build(:user)
    document.currency_id = 123

    expect(document).to be_invalid
  end

  it "is invalid without date" do
    document = FactoryGirl.create :document
    document.date = nil

    expect(document).to be_invalid
  end

  it "is invalid with wrong date" do
    document = FactoryGirl.create :document
    document.date = "some date"

    expect(document).to be_invalid
  end

  it "is invalid with wrong currency" do
    document = create :document
    document.user = create :user
    document.currency = create :currency

    expect(document).to be_invalid
  end

  it "sets todays date for empty date" do
    document = FactoryGirl.create :document, date: nil

    expect(document.date).to eq Date.today
  end

  it "sets custom date" do
    document = FactoryGirl.create :document, date: "1 January 2012"

    expect(document.date).to eq Date.parse("1 January 2012")
  end

  it 'is valid with users category' do
    document = create :document
    user = create :user
    document.user = user
    category = create :category
    user.categories.append category
    document.category = category

    expect(document).to be_valid
  end

  it 'is invalid with non-users category' do
    document = create :document
    document.user = create :user
    document.category = create :category

    expect(document).to be_invalid
  end

  it 'is valid with blank category' do
    document = create :document, category: nil

    expect(document).to be_valid
  end

  it 'is invalid with nonexistent category id' do
    document = build :document, user: build(:user)
    document.category_id = 123

    expect(document).to be_invalid
  end

  describe "current state" do
    it "is in new in the beginning" do
      document = FactoryGirl.create :document

      expect(document).to be_new
    end

    it "is in process after processing" do
      document = create :document, :new
      document.handle!

      expect(document).to be_in_process
    end

    it "is in pending approval after requesting approval" do
      document = create :document, :in_process
      document.request_approval!

      expect(document).to be_pending_approval
    end

    it "is in ready after approving" do
      document = create :document, :pending_approval
      document.approve!

      expect(document).to be_ready
    end

    it "is in rejected after rejecting" do
      document = create :document, :pending_approval
      document.reject!

      expect(document).to be_rejected
    end

    it "is againg in process after amending" do
      document = create :document, :rejected
      document.amend!

      expect(document).to be_in_process
    end
  end

end
