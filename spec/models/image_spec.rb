require 'rails_helper'

RSpec.describe Image, type: :model do
  it 'does not allow size of image more than 5 MB'  do
    image = build :image_invalid

    expect(image).to be_invalid
  end

  it 'sets angle property'  do
    image = create :image_valid

    image.rotate_image(90)

    expect(image.angle).to eq 90
  end

  it 'calls recreate_versions!'  do
    image = create :image_valid
    allow(image.image).to receive(:recreate_versions!)

    image.rotate_image(90)

    expect(image.image).to have_received(:recreate_versions!)
  end

  it 'has file name'  do
    image = create :image_valid

    expect(image.filename).to be
  end

end
