require "rails_helper"

RSpec.describe ImagesController, type: :routing do
  describe "routing" do

    it "routes to #update via PUT" do
      expect(:put => "/images/1").to route_to("images#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/images/1").to route_to("images#update", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/images").to route_to("images#create")
    end
  end
end
