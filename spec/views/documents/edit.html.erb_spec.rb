require 'rails_helper'

RSpec.describe "documents/edit", type: :view do
  before do
    @ability = Object.new
    @ability.extend(CanCan::Ability)
    controller.stub(:current_ability) { @ability }
  end

  it "renders the edit document form" do
    @ability.can :update, Document
    image = create :image
    sign_in create :user
    assign :document, image.document

    render

    expect(view.content_for(:tools)).to include('form class="edit_document"')
  end
end
