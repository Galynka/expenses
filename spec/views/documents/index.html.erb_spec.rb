require 'rails_helper'

RSpec.describe "documents/index", type: :view do
  before do
    @ability = Object.new
    @ability.extend(CanCan::Ability)
    controller.stub(:current_ability) { @ability }
  end

  it "renders a list of documents" do
    @ability.can :manage, Document
    assign(:documents, Kaminari.paginate_array([
      create(:document, description: 'test', image: create(:image))
    ]).page(1))

    render

    within "#posts" do
      expect(rendered).to have_selector("ul")
    end
  end
end
