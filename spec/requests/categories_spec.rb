require 'rails_helper'

RSpec.describe "Categories", type: :request do
  before do
    Rails.application.env_config["devise.mapping"] = Devise.mappings[:user] # If using Devise
    Rails.application.env_config["omniauth.auth"] = OmniAuth.config.mock_auth[:facebook]
    FactoryGirl.create :role, name: 'guest'
  end

  describe "GET /categories" do
    it "works! (now write some real specs)" do
      FactoryGirl.create :user, provider: 'facebook', uid: '123545'
      get user_omniauth_authorize_path(provider: :facebook)
      get user_omniauth_callback_path :facebook

      get categories_path

      expect(response).to have_http_status(200)
    end
  end
end
