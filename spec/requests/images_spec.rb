require 'rails_helper'

RSpec.describe "Images", type: :request do
  before do
    Rails.application.env_config["devise.mapping"] = Devise.mappings[:user] # If using Devise
    Rails.application.env_config["omniauth.auth"] = OmniAuth.config.mock_auth[:facebook]
    FactoryGirl.create :role, name: 'guest'
  end

  describe "POST /create" do
    it "it creates an image and shows result of document creating" do
      role = create :role, name: 'admin'
      get user_omniauth_authorize_path(provider: :facebook)
      get user_omniauth_callback_path :facebook
      User.last.update role: role
      test_upload = [Rack::Test::UploadedFile.new(File.open(Rails.root.join('spec/fixtures/images/test.png'), 'r'))]
      post(images_path, {image: { image: test_upload }})

      expect(response).to redirect_to(edit_document_path(assigns(:collection).first.document))
      follow_redirect!
      expect(response.body).to include("Document was created")
    end
  end
end
