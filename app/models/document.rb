class Document < ActiveRecord::Base
  include Workflow
  belongs_to :currency
  belongs_to :category
  has_many :categories, through: :user
  belongs_to :user
  has_many :currencies, through: :user
  paginates_per 12
  belongs_to :image, dependent: :destroy
  accepts_nested_attributes_for :image
  monetize :amount_subunit, :as => "amount", :allow_nil => true
  before_validation :set_date_to_now
  validates :currency_id, inclusion: { in: ->(document) { document.currency_ids },
    message: "%{value} must be in currencies list" }, allow_nil: true
  validates :category_id, inclusion: { in: ->(document) { document.category_ids },
                                       message: "%{value} must be your category" }, allow_nil: true

  validate do
    self.errors[:date] << "must be a valid date" unless (Date.parse(self.date.to_s) rescue false)
  end

  def set_date_to_now
    if !self.persisted?
      self.date ||= Date.today
    end
  end

  workflow do
    state :new do
      event :handle, transitions_to: :in_process
    end
    state :in_process do
      event :request_approval, transitions_to: :pending_approval
    end
    state :pending_approval do
      event :approve, transitions_to: :ready
      event :reject, transitions_to: :rejected
    end
    state :rejected do
      event :amend, transitions_to: :in_process
    end
    state :ready
  end
end
