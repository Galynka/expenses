class Image < ActiveRecord::Base
  mount_uploader :image, ImageUploader
  validate :image_size
  has_one :document
  attr_accessor :angle
  attr_accessor :user_id

  def rotate_image(angle)
    self.angle = angle
    self.image.recreate_versions!
  end

  after_create :create_document

  def filename
    image.filename
  end

  private

  # Validates the size of an uploaded image.
  def image_size
    if image.size > 5.megabytes
      errors.add(:image, 'should be less than 5MB')
    end
  end

  def create_document
    if self.document.blank?
      self.create_document! user_id: self.user_id
    end
  end
end
