json.array!(@documents) do |document|
  json.extract! document, :id, :date, :amount, :currency_id, :category_id, :description, :image_id
  json.url document_url(document, format: :json)
end
