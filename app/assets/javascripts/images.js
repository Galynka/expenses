function browse_image(){
  document.getElementById("image_image").click();
};

function submit_form(file_field){
  var size_in_megabytes = file_field.files[0].size/1024/1024;
  if (size_in_megabytes > 5) {
    alert('Image was not submitted. Maximum image size is 5MB. Please choose a smaller file.');
    return false;
  }
  file_field.form.submit();
  return true;
};

$(document).on('ready page:load', function(event) {
  $("#rotate_clockwise").click(function(){
    rotation.setRotateAngle(90);
    rotation.rotate();
  });
  $("#rotate_counter_clockwise").click(function(){
    rotation.setRotateAngle(-90);
    rotation.rotate();
  });
  $("#image_angle_form").bind('ajax:send', function() {
     rotation.clearValue();
  });

  rotation = function(){
    var displayAngle = 0;
    var angleField = document.getElementById('angle_field');
    var setRotateAngle = function(newAngle){
      var currentAngle = parseInt(angleField.value);
        angleField.value = (currentAngle + newAngle).toString();
        displayAngle += newAngle;
    };
    var rotate = function(){
      $('.edit_pic').css({transform : 'rotate(' + displayAngle + 'deg)'});
    }
    var clearValue = function(){
      angleField.value = "0";
    }
    return{setRotateAngle:setRotateAngle, rotate:rotate, clearValue:clearValue}
  }();
});

