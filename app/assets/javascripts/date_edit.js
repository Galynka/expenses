$(document).on('ready page:load', function(event) {
  $(function() {
    $("#document_date").datepicker({
      dateFormat:'d MM yy',
      showOtherMonths: true,
      selectOtherMonths: true,
      changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      yearRange: '2010:2040' 
    });
  });
});
