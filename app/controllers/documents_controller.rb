class DocumentsController < ApplicationController
  before_action :set_document, except: [:index, :new]
  load_and_authorize_resource

  # GET /documents
  # GET /documents.json
  def index
    @documents = Document.page params[:page]
  end

# GET /documents/new
  def new
    image = Image.create(user_id: current_user.id)
    @document = image.document
    render 'edit'
  end

  # GET /documents/1/edit
  def edit
  end

  # POST /documents
  # POST /documents.json
  def create
    @document = Document.new(document_params)
    @document.user_id = current_user.id
    authorize! :create, @document
    respond_to do |format|
      if @document.save
        format.html { redirect_to documents_path, notice: 'Document was successfully created.' }
        format.json { render :show, status: :created, location: @document }
      else
        format.html { render :new }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /documents/1
  # PATCH/PUT /documents/1.json
  def update
    respond_to do |format|
      if @document.update(document_params)
        format.html { redirect_to documents_url}
      else
        format.html { render :edit }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/1
  # DELETE /documents/1.json
  def destroy
    @document.destroy
    respond_to do |format|
      format.html { redirect_to documents_url, notice: 'Document was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def handle
    authorize! :handle, @document
    @document.handle!
    head :ok
  end

  def request_approval
    authorize! :request_approval, @document
    @document.request_approval!
    head :ok
  end

  def approve
    authorize! :approve, @document
    @document.approve!
    head :ok
  end

  def reject
    authorize! :reject, @document
    @document.reject!
    head :ok
  end

  def amend
    authorize! :amend, @document
    @document.amend!
    head :ok
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_document
      @document = Document.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def document_params
      params.require(:document).permit(:date, :amount, :currency_id, :category_id, :description, image_attributes: [:id ,:image] )
    end
end
