class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :authenticate_user!, except: :sign_in

  def after_sign_in_path_for(*args)
    documents_url
  end

  def after_sign_out_path_for(*args)
    sign_in_url
  end

  rescue_from CanCan::AccessDenied do |exception|
    access_denied(exception)
  end

  def access_denied(exception)
    redirect_to root_url, flash: {error: exception.message}
  end
end
