# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

["guest","employee", "customer", "admin"].each do |role|
  Role.find_or_create_by(name: role)
end

present_users = User.all
role_name = Role.find_by_name('admin')
present_users.each do |user|
  user.role = role_name
  user.save
end
